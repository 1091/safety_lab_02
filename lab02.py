import re
import argparse


def split_hex_string_by_two(string):
    """Returns string exploded by two characters
    For example 'a6b245' -> ['a6', 'b2, '45']
    (string with hexadecimals)
    """
    return re.findall(r"[a-f0-9]{2}", string)


def read_messages_from_file(filename):
    file_ = open(filename, 'r')
    lines = file_.readlines()
    lines = map(lambda line: re.sub("\s", "", line), lines)
    lines = filter(None, lines)
    return lines


def from_hex_to_int_messages(messages_hex):
    messages_int = []
    for message in messages_hex:
        messages_int.append(map(lambda hex_: int(hex_, 16), message))
    return messages_int


def xor_message_by_message(messages_int):
    xored_messages = []
    for i in range(len(messages_int)):
        xored_line = []
        for j in range(i+1, len(messages_int)):
            lst = []
            for k in range(min([len(messages_int[i]), len(messages_int[j])])):
                xored = messages_int[i][k] ^ messages_int[j][k]
                lst.append(xored)
            xored_line.append(lst)
        if xored_line:
            xored_messages.append(xored_line)
    return xored_messages


def add_line_with_numbers_of_letters_in_columns(messages_xored):
    for message in messages_xored:
        length_list = map(len, message)
        last_line = [0] * max(length_list)
        for line in message:
            index = 0
            for character in line:
                if ((ord('A') <= character <= ord('Z')) or
                        (ord('a') <= character <= ord('z'))):
                    last_line[index] += 1
                index += 1
        message.append(last_line)


def find_space_probable_indexes(messages_xored):
    space_probable_indexes = []
    for message in messages_xored:
        last_line = message[-1]
        lst = []
        index = 0
        for number in last_line:
            if number == max(last_line):
                lst.append(index)
            index += 1
        space_probable_indexes.append(lst)
    return space_probable_indexes


def find_key(messages_int, space_probable_indexes):
    """Finds stream cipher key by given messages (represented as integers)
    and by the indexes, which say where probably space character in plaintext
    """
    MAGICK_NUMBER = 10
    key = [0] * MAGICK_NUMBER
    message_index = 0
    for index_xored in space_probable_indexes:
        for index in index_xored:
            if index >= len(key):
                appendix = [0] * (index - MAGICK_NUMBER + 1)
                key.extend(appendix)
            key[index] = messages_int[message_index][index] ^ ord(' ')
        message_index += 1
    acceptable = False
    while not acceptable:
        if key[-1] == 0:
            key.pop()
        else:
            acceptable = True
    return key


def attempt_to_decrypt(key, message):
    """Attempts to decrypt by given key and message (lists of integers)
    and returns string with plain text
    """
    if len(key) > len(message):
        key_slice = key[:len(message)]
        return ''.join(map(lambda message, key: chr(message ^ key),
                           message, key_slice))
    elif len(key) < len(message):
        message_slice = message[:len(key)]
        return ''.join(map(lambda message, key: chr(message ^ key),
                           message_slice, key))
    else:
        return ''.join(map(lambda message, key: chr(message ^ key),
                           message, key))


def find_key_by_plain_and_crypted(plain_text, crypted):
    plain_list = []
    for character in plain_text:
        plain_list.append(character)
    plain_list = map(ord, plain_list)
    key = map(
        lambda plain, crypted: plain ^ crypted,
        plain_list,
        crypted
    )
    return key


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', help="file with messages in hexadecimal")
    args = parser.parse_args()
    messages_hex = read_messages_from_file(args.input_file)
    messages_hex = map(split_hex_string_by_two, messages_hex)
    messages_int = from_hex_to_int_messages(messages_hex)
    messages_xored = xor_message_by_message(messages_int)
    add_line_with_numbers_of_letters_in_columns(messages_xored)
    space_probable_indexes = find_space_probable_indexes(messages_xored)
    key = find_key(messages_int, space_probable_indexes)

    print "stream cipher key:\n", key
    print "plain text:"
    decrypted = []
    for message in messages_int:
        decrypted.append(attempt_to_decrypt(key, message))
    for message in decrypted:
        print message
    answered = False
    while not answered:
        plain_text = raw_input(
            "please input last message, but more humanreadable\n"
        )
        if len(plain_text) != len(messages_int[-1]):
            print "wrong size!"
        else:
            answered = True
    key = find_key_by_plain_and_crypted(plain_text, messages_int[-1])
    print "more accurate key:"
    print key
    print "more accurate decrypt:"
    decrypted = []
    for message in messages_int:
        decrypted.append(attempt_to_decrypt(key, message))
    for message in decrypted:
        print message

    lenghts = map(len, messages_int)
    longest_messages = []
    index = 0
    for length in lenghts:
        if length == max(lenghts):
            longest_messages.append(index)
        index += 1
    print "longest lines: "
    for index in longest_messages:
        print "[" + str(index) + "]^" + decrypted[index]
    answered = False
    while not answered:
        answer = raw_input(
            "choose one of the longest lines (enter the number) : "
        )
        match = re.match("[0-9]+", answer)
        if match and int(match.group()) in longest_messages:
            answered = True
    answer = int(match.group())
    answered = False
    while not answered:
        plain_text = raw_input(
            "find it " +
            "in internet and enter right there\n" +
            "be careful and don't forget about spaces\n" +
            "especially in the start of line\n"
        )
        if len(plain_text) != len(messages_int[answer]):
            print "wrong length!"
        else:
            answered = True
    key = find_key_by_plain_and_crypted(plain_text, messages_int[answer])
    print "full stream cipher key:\n", key
    print "full encrypted text:"
    decrypted = []
    for message in messages_int:
        decrypted.append(attempt_to_decrypt(key, message))
    for message in decrypted:
        print message

    decrypted_file = open("decrypted", 'w')
    for line in decrypted:
        decrypted_file.write(line + "\n")
